{% from 'openldap/map.jinja' import openldap with context %}

ldap-utils:
  pkg.installed:
    - name: ldap-utils

ldap-server:
  pkg.installed:
    - name: {{ openldap.server_pkg }}
    - refresh: True
  service.running:
    - name: {{ openldap.service }}
    - enable: True

/tmp/srv_cred.ldif:
  file.managed:
    - source: salt://openldap/files/srv_cred.ldif
    - template: jinja
    - user: root
    - group: root
    - mode: 644

create-admin-pass:
  cmd.run:
    - name: ldapmodify -H ldapi:// -Y EXTERNAL -f /tmp/srv_cred.ldif
    - require:
      - pkg: ldap-utils

delete-tmp-ldif:
  file.absent:
    - name: /tmp/srv_cred.ldif
