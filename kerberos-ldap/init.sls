krb5-kdc-ldap:
  pkg.installed

{% if not salt['file.file_exists']('/usr/share/doc/krb5-kdc-ldap/kerberos.schema') %}
gzip -d -f /usr/share/doc/krb5-kdc-ldap/kerberos.schema.gz:
  cmd.run:
    - user: root
    - require:
      - pkg: krb5-kdc-ldap
{% endif %}

cp /usr/share/doc/krb5-kdc-ldap/kerberos.schema /etc/ldap/schema/:
  cmd.run:
    - user: root

/tmp/ldif_output:
  file.directory:
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

/tmp/schema_convert.conf:
  file.managed:
    - source: salt://kerberos-ldap/files/schema_convert.conf
    - template: jinja
    - user: root
    - group: root
    - mode: 644

slapcat -f /tmp/schema_convert.conf -F /tmp/ldif_output -n0 -s "cn={12}kerberos,cn=schema,cn=config" > /tmp/cn=kerberos.ldif:
  cmd.run:
    - user: root

Alter kerberos cn_kerberos:
  cmd.script:
    - name: db_script.sh
    - source: salt://kerberos-ldap/files/db_script.sh
    - user: root
