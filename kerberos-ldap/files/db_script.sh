sed -i 's/dn: cn={12}kerberos,cn=schema,cn=config/dn: cn=kerberos,cn=schema,cn=config/' /tmp/cn=kerberos.ldif
sed -i 's/cn: {12}kerberos/cn: kerberos/' /tmp/cn=kerberos.ldif
sed -i '/^structuralObjectClass/d' /tmp/cn=kerberos.ldif
sed -i '/^entryUUID/d' /tmp/cn=kerberos.ldif
sed -i '/^creatorsName/d' /tmp/cn=kerberos.ldif
sed -i '/^createTimestamp/d' /tmp/cn=kerberos.ldif
sed -i '/^entryCSN/d' /tmp/cn=kerberos.ldif
sed -i '/^modifiersName/d' /tmp/cn=kerberos.ldif
sed -i '/^modifyTimestamp/d' /tmp/cn=kerberos.ldif
